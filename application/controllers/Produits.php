<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produits extends CI_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->model('produit');
		$this->load->helper('html');
		$this->load->helper('url');
	}

	public function index() {
		$data['produits'] = $this->produit->listerProduit();
		$data['titre'] = 'Liste des produits';
		$data['content'] = 'listeProduits';
		$this->load->vars($data);
		$this->load->view('template');
	}
	public function listeProduits() {
		$data['titre'] = 'Liste des résultats';
		$data['produits'] = $this->produit->listerProduit();
		$data['content'] = 'listeProduits';
		$this->load->vars($data);
		$this->load->view('template');
	}

	public function recherche() {
		$this->load->helper('form');

		if (isset($_POST['code']) || isset($_POST['nom'])) {
			$data['produits'] = $this->produit->recherche($_POST['code'], $_POST['nom']);
			$data['titre'] = 'Liste des résultats';
			$data['recherche'] = true;
			$data['content'] = 'listeProduits';
		} else {
			$data['content'] = 'recherche';
		}

		$this->load->vars($data);
		$this->load->view('template');
	}

	public function modifierProduits($code) {
		$this->load->helper('form');
		$data = $this->produit->recuperer($code);
		$data['content'] = 'modification';
		$this->load->vars($data);
		$this->load->view('template');
	}
		
	public function consulter($code) {
		$data = $this->produit->recuperer($code);
		$data['content'] = 'consulter';
		$this->load->vars($data);
		$this->load->view('template');
	}

	public function ajouter() {
		$data['content'] = 'ajout';
		$this->load->vars($data);
		$this->load->view('template');
	}
}


