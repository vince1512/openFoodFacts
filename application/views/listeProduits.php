<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<h1><?php echo $titre; ?></h1>

    	<a class="btn btn-primary btn-lg" href="<?php echo site_url('produits/recherche'); ?>">Recherche</a> 
    <?php if($titre == "listeProduits") :?>
    	<a class="btn btn-secondary btn-lg" href="<?php echo site_url(''); ?>">Liste Complete</a>
    <?php endif; ?>
    <a class="btn btn-info btn-lg" href="<?php echo site_url('produits/ajouter'); ?>">Ajouter produit</a>
    
<?php $compteur = 0; ?>
<hr>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Nom</th>
	<th scope="col">Marque</th>
        <th scope="col">Pays</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
	<?php foreach ($produits as $produit): ?>
        <tr>
            <th scope="row"><?php echo $produit['code']; ?></th>
            <td><?php echo $produit['nom']; ?></td>
	    <td><?php echo $produit['marque']; ?></td>
	    <td><?php echo $produit['pays']; ?></td>
            <td><a class="btn btn-sm btn-primary" href="<?php echo site_url('produits/consulter/' . $produit['code']); ?>">Consulter</a></td>
        </tr>
	<?php $compteur = $compteur + 1; ?>
	<?php endforeach; ?>
	<p> Il y a <?php echo $compteur ?> resultats </p>
    </tbody>
</table>
