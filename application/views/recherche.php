<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <h1>Rechercher un produit</h1>

<?php echo form_open('produits/recherche'); ?>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="codeProduit">Code du produit</label>
            <input type="text" class="form-control" id="codeProduit" name="code" placeholder="Code">
            <br>
        </div>
        <div class="form-group col-md-6">
            <label for="nomProduit">Nom du produit</label>
            <input type="text" class="form-control" id="nomProduit" name="nom" placeholder="Nom">
            <br>
        </div>
    </div>
    <div class="form-row">
        <div class="form-row col-md-4">
            <label for="marqueProduit"> Marque du produit</label>
            <select class="form-control">
                <option>Marque 1</option>
                <option>Marque 2</option>
            </select>
            <br>
        </div>
        <div class="form-row col-md-4">
            <label for="paysProduit"> Pays du produit</label>
            <select class="form-control">
                <option>Pays 1</option>
                <option>Pays 2</option>
            </select>
            <br>
        </div>
        <div class="form-row col-md-4">
            <label for="categorieProduit"> Categorie du produit</label>
            <select class="form-control">
                <option>Categorie 1</option>
                <option>Categorie 2</option>
            </select>
        </div>
    </div>
    <br>
    <div>
        <label> Produit qui ne contient pas (allergenes)</label><br>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"> Allergene 1
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"> Allergene 2
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"> Allergene 3
            </label>
        </div>
    </div>
    <br>
    <div>
        <label>Produit qui ne contient pas (additifs)</label><br>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"> Additif 1
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"> Additif 2
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"> Additif 3
            </label>
        </div>
    </div>
    <br>
    <button type="submit" class="btn btn-primary btn-lg">Rechercher</button>
    <a class="btn btn-primary btn-lg" href="<?php echo site_url('produits/listeProduits'); ?>">Voir liste complete</a>
<?php echo form_close(); ?>
