<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php function afficherTable($titre, $tableau) { ?>
    <h2><?php echo $titre; ?></h2>

	<?php if (empty($tableau)): ?>
        <div class="alert alert-warning" role="alert">
            Ce tableau est vide.
        </div>
	<?php else: ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Information</th>
                <th scope="col">Valeur</th>
            </tr>
            </thead>
            <tbody>
			<?php foreach ($tableau as $k => $v): ?>
               			 <tr>
                    			<th scope="row"><?php echo $k; ?></th>
					<?php if($k != "marque" && $k != "pays" && $k != "categorie"):?>
				 		<td><?php echo $v ?? 'inconnu'; ?></td>
					<?php else : ?>
						<select class="form-control" id="marqueListe"> 
     				 			<option selected="<?php echo $v?>"><?php echo $v?></option>
						</select>
					<?php endif; ?>
                		</tr>
			<?php endforeach; ?>
            </tbody>
        </table>
	<?php endif; ?>
<?php } ?>

<?php function afficherListe($titre, $tableau) { ?>
    <h2><?php echo $titre; ?></h2>

	<?php if (empty($tableau)): ?>
        <div class="alert alert-warning" role="alert">
            Ce tableau est vide.
        </div>
	<?php else: ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Nom</th>
            </tr>
            </thead>
            <tbody>
			<?php foreach ($tableau as $k => $v): ?>
                <tr>
                    <td><input type="text" class="form-control" value="<?php echo $v ?>"></td> 

                </tr>
			<?php endforeach; ?>
            </tbody>
        </table>
	<?php endif; ?>
<?php } ?>
<h1>Modifier <strong><?php echo $produit['nom']; ?></strong></h1>
<a class="btn btn-info btn-lg" href="<?php echo site_url('produits/consulter/' . $produit['code']); ?>">Retour</a>
<button type="submit" class="btn btn-primary btn-lg" href="<?php echo site_url('produits/consulter/' . $produit['code']); ?>">Enregistrer</button>
<div class="row">
    <div class="col">
		<?php afficherTable('Détails du produit', $produit); ?>
		<?php afficherTable('Liste des sels minéraux', $selsmineraux); ?>
		<?php afficherTable('Liste des vitamines', $vitamines); ?>
    </div>
    <div class="col">
		<?php afficherListe('Liste des ingrédients', $ingredients); ?>
		<?php afficherListe('Liste des additifs', $additifs); ?>
		<?php afficherListe('Liste des allergenes', $allergenes); ?>
    </div>
</div>
    </div>
