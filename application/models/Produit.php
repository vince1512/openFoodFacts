<?php

class Produit extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->database();
	}

	public function listerProduit() {
		$query = $this->db->query("SELECT code, nom, marque, pays FROM openfoodfacts._produit order by code");
		return $query->result_array();
	}

	public function recherche($code, $nom) {
		$basesql = "SELECT code, nom, marque, pays FROM openfoodfacts._produit WHERE ";
		$sql = $basesql;

		if ($code != null) {
			$sql .= "code = '$code'";
		}

		if ($nom != null) {
			if ($sql != $basesql) {
				$sql .= ' and ';
			}

			$sql .= "upper(nom) like upper('%$nom%')";
		}

		if ($sql == $basesql) {
			$sql = "SELECT code, nom, marque, pays FROM openfoodfacts._produit order by code";
		}

		$query = $this->db->query($sql);
		return $query->result_array();
	}
	

	public function recuperer($code) {
		$queryProduit = $this->db->query("select * from openfoodfacts._produit where code = '$code'");
		$querySelsMineraux = $this->db->query("select * from openfoodfacts._selsMineraux where code_produit = '$code'");
		$queryVitamines = $this->db->query("select * from openfoodfacts._vitamines where code_produit = '$code'");
		$queryIngredients = $this->db->query("select ingredient from openfoodfacts._ingredient where code_produit = '$code'");
		$queryAdditifs = $this->db->query("select additif from openfoodfacts._additif where code_produit = '$code'");
		$queryAllergenes = $this->db->query("select allergene from openfoodfacts._allergene where code_produit = '$code'");

		return array(
			'produit' => $queryProduit->row_array(),
			'selsmineraux' => $querySelsMineraux->row_array(),
			'vitamines' => $queryVitamines->row_array(),
			'ingredients' => array_column($queryIngredients->result_array(), 'ingredient'),
			'additifs' => array_column($queryAdditifs->result_array(), 'additif'),
			'allergenes' => array_column($queryAllergenes->result_array(), 'allergene')
		);
	}
}
